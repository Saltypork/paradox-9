﻿Class 001
 Name "Warrior"
 Notes "<職業ランク 1>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 30,15,10,20,15>"
Class 002
 Name "Swordmaster"
 Notes "<要求経験職 1-10>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 40,25,20,30,25>"
Class 003
 Name "***ダミー"
Class 004
 Name "***ダミー"
Class 005
 Name "Holy Knight"
 Notes "<要求経験職 1-10,44-10>\r\n<職業ランク 2>\r\n<最大TP増加 15%>\r\n<メモライズ容量補正 35,30,25,40,30>"
Class 006
 Name "***ダミー"
Class 007
 Name "Magiknight"
 Notes "<要求経験職 1-10,33-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 35,25,35,20,30>"
Class 008
 Name "***ダミー"
Class 009
 Name "Dragon Knight"
 Notes "<要求経験職 1-10>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 35,25,20,30,30>"
Class 010
 Name "***ダミー"
Class 011
 Name "***ダミー"
Class 012
 Name "***ダミー"
Class 013
 Name "***ダミー"
Class 014
 Name "***ダミー"
Class 015
 Name "Power Fighter"
 Notes "<要求経験職 1-10>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 40,20,20,40,20>\r\n<踏みとどまり 30%>"
Class 016
 Name "***ダミー"
Class 017
 Name "Berserker"
 Notes "<要求経験職 1-10>\r\n<転職アイテム 604>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 40,20,20,30,20>"
Class 018
 Name "***ダミー"
Class 019
 Name "Tao Swordsman"
 Notes "<要求経験職 1-10,42-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 35,30,30,20,35>\r\n<魔法反撃 25%>"
Class 020
 Name "Kuji-In Sword God"
 Notes "<要求経験職 19-10>\r\n<転職アイテム 624>\r\n<職業ランク 3>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 45,40,40,30,45>\r\n<魔法反撃 50%>"
Class 021
 Name "***ダミー"
Class 022
 Name "Martial Artist"
 Notes "<職業ランク 1>\r\n<メモライズ容量補正 25,20,15,15,20>"
Class 023
 Name "Overpowering Fist"
 Notes "<要求経験職 22-10>\r\n<職業ランク 2>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 35,30,25,25,30>"
Class 024
 Name "***ダミー"
Class 025
 Name "***ダミー"
Class 026
 Name "Magus Fist"
 Notes "<要求経験職 22-10,33-10>\r\n<職業ランク 2>\r\n<メモライズ容量補正 35,25,30,20,30>\r\n<魔法反撃 25%>"
Class 027
 Name "***ダミー"
Class 028
 Name "Paladin"
 Notes "<要求経験職 22-10,44-10>\r\n<職業ランク 2>\r\n<メモライズ容量補正 35,25,25,40,35>"
Class 029
 Name "***ダミー"
Class 030
 Name "Battle Master"
 Notes "<要求経験職 1-10,22-10>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 40,30,20,40,30>"
Class 031
 Name "***ダミー"
Class 032
 Name "***ダミー"
Class 033
 Name "Magician"
 Notes "<職業ランク 1>\r\n<最大TP減少 30%>\r\n<メモライズ容量補正 10,15,30,15,25>"
Class 034
 Name "Black Mage"
 Notes "<要求経験職 33-10>\r\n<職業ランク 2>\r\n<最大TP減少 20%>\r\n<メモライズ容量補正 20,25,40,25,35>"
Class 035
 Name "***ダミー"
Class 036
 Name "***ダミー"
Class 037
 Name "***ダミー"
Class 038
 Name "Time Mage"
 Notes "<要求経験職 33-10>\r\n<職業ランク 2>\r\n<最大TP減少 20%>\r\n<メモライズ容量補正 20,30,35,25,30>"
Class 039
 Name "***ダミー"
Class 040
 Name "Summoner"
 Notes "<要求経験職 33-10>\r\n<転職アイテム 605>\r\n<職業ランク 2>\r\n<最大TP減少 30%>\r\n<メモライズ容量補正 20,25,40,20,40>"
Class 041
 Name "***ダミー"
Class 042
 Name "Taoist"
 Notes "<要求経験職 33-10>\r\n<転職アイテム 603>\r\n<職業ランク 2>\r\n<最大TP減少 10%>\r\n<メモライズ容量補正 25,35,35,20,35>"
Class 043
 Name "Tao Master"
 Notes "<要求経験職 42-10>\r\n<転職アイテム 625>\r\n<職業ランク 3>\r\n<メモライズ容量補正 35,45,45,30,45>"
Class 044
 Name "Priest"
 Notes "<職業ランク 1>\r\n<最大TP減少 10%>\r\n<メモライズ容量補正 15,15,25,20,25>"
Class 045
 Name "White Mage"
 Notes "<要求経験職 44-10>\r\n<職業ランク 2>\r\n<最大TP減少 5%>\r\n<メモライズ容量補正 25,25,35,30,35>"
Class 046
 Name "***ダミー"
Class 047
 Name "Sage"
 Notes "<要求経験職 33-10,44-10>\r\n<職業ランク 2>\r\n<最大TP減少 10%>\r\n<メモライズ容量補正 25,30,40,25,40>"
Class 048
 Name "Cosmic Magus"
 Notes "<要求経験職 34-10,38-10,45-10,47-10>\r\n<転職アイテム 625>\r\n<職業ランク 3>\r\n<メモライズ容量補正 35,40,50,35,50>"
Class 049
 Name "***ダミー"
Class 050
 Name "***ダミー"
Class 051
 Name "***ダミー"
Class 052
 Name "Hunter"
 Notes "<職業ランク 1>\r\n<メモライズ容量補正 20,25,15,15,30>\r\n<森地形強化>"
Class 053
 Name "Bowmaster"
 Notes "<要求経験職 52-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 30,35,25,25,40>\r\n<森地形強化>"
Class 054
 Name "***ダミー"
Class 055
 Name "***ダミー"
Class 056
 Name "Gunner"
 Notes "<要求経験職 52-10>\r\n<転職アイテム 606>\r\n<職業ランク 2>\r\n<メモライズ容量補正 30,35,25,20,35>"
Class 057
 Name "***ダミー"
Class 058
 Name "***ダミー"
Class 059
 Name "Monster Tamer"
 Notes "<要求経験職 52-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 30,35,25,30,35>\r\n<仲間加入倍率 150%>"
Class 060
 Name "***ダミー"
Class 061
 Name "***ダミー"
Class 062
 Name "Thief"
 Notes "<職業ランク 1>\r\n<メモライズ容量補正 20,20,20,20,30>\r\n<スティール成功率 150%>"
Class 063
 Name "Master Thief"
 Notes "<要求経験職 62-10>\r\n<職業ランク 2>\r\n<メモライズ容量補正 30,30,30,30,40>\r\n<スティール成功率 200%>"
Class 064
 Name "***ダミー"
Class 065
 Name "Ninja"
 Notes "<要求経験職 62-10>\r\n<転職アイテム 603>\r\n<職業ランク 2>\r\n<メモライズ容量補正 35,35,20,20,25>"
Class 066
 Name "***ダミー"
Class 067
 Name "***ダミー"
Class 068
 Name "***ダミー"
Class 069
 Name "***ダミー"
Class 070
 Name "***ダミー"
Class 071
 Name "***ダミー"
Class 072
 Name "Guard"
 Notes "<転職アイテム 602>\r\n<職業ランク 1>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 25,20,10,20,15>\r\n<市街地形強化>"
Class 073
 Name "Palace Knight"
 Notes "<要求経験職 1-10,72-10>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 35,30,20,30,25>\r\n<市街地形強化>"
Class 074
 Name "***ダミー"
Class 075
 Name "***ダミー"
Class 076
 Name "***ダミー"
Class 077
 Name "Hero of Justice"
 Notes "<要求経験職 72-10>\r\n<転職アイテム 609>\r\n<職業ランク 2>\r\n<メモライズ容量補正 35,35,20,25,30>\r\n<HP減少時強化 20,50%>\r\n<オーバーソウル 25%>"
Class 078
 Name "***ダミー"
Class 079
 Name "Merchant"
 Notes "<転職アイテム 601>\r\n<職業ランク 1>\r\n<メモライズ容量補正 10,25,10,20,20>\r\n<獲得金額倍率 150%>"
Class 080
 Name "Black Marketeer"
 Notes "<要求経験職 79-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 20,35,20,30,30>\r\n<獲得金額倍率 200%>"
Class 081
 Name "***ダミー"
Class 082
 Name "Item User"
 Notes "<要求経験職 79-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 20,35,20,25,35>"
Class 083
 Name "***ダミー"
Class 084
 Name "Adventurer"
 Notes "<要求経験職 52-10,62-10>\r\n<職業ランク 2>\r\n<最大TP増加 15%>\r\n<メモライズ容量補正 35,35,20,35,35>\r\n<砂漠地形強化>\r\n<洞窟地形強化>"
Class 085
 Name "***ダミー"
Class 086
 Name "Fortune Teller"
 Notes "<転職アイテム 610>\r\n<職業ランク 1>\r\n<最大TP減少 10%>\r\n<メモライズ容量補正 10,20,20,10,30>\r\n<獲得アイテム倍率 150%>\r\n<スロットチャンス 1>"
Class 087
 Name "Gambler"
 Notes "<要求経験職 86-10>\r\n<職業ランク 2>\r\n<メモライズ容量補正 25,30,20,25,40>\r\n<獲得アイテム倍率 200%>\r\n<スロットチャンス 2>"
Class 088
 Name "***ダミー"
Class 089
 Name "Card Battler"
 Notes "<要求経験職 86-10>\r\n<職業ランク 2>\r\n<メモライズ容量補正 25,30,25,20,40>"
Class 090
 Name "***ダミー"
Class 091
 Name "***ダミー"
Class 092
 Name "Flirt"
 Notes "<職業ランク 1>\r\n<最大TP減少 30%>\r\n<メモライズ容量補正 10,10,10,10,30>\r\n<行動変化 28-50>\r\n<スロットチャンス 1>"
Class 093
 Name "Dancer"
 Notes "<要求経験職 92-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 25,35,30,20,30>"
Class 094
 Name "***ダミー"
Class 095
 Name "Minstrel"
 Notes "<要求経験職 92-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 25,35,25,25,30>"
Class 096
 Name "***ダミー"
Class 097
 Name "Prostitute"
 Notes "<要求経験職 92-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 20,35,20,20,35>"
Class 098
 Name "***ダミー"
Class 099
 Name "***ダミー"
Class 100
 Name "Superstar"
 Notes "<要求経験職 93-10,95-10>\r\n<職業ランク 2>\r\n<最大TP増加 15%>\r\n<メモライズ容量補正 35,50,40,35,50>"
Class 101
 Name "Battle Fucker"
 Notes "<要求経験職 97-10>\r\n<転職アイテム 611>\r\n<職業ランク 2>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 25,35,20,35,35>"
Class 102
 Name "***ダミー"
Class 103
 Name "Scholar"
 Notes "<転職アイテム 612>\r\n<職業ランク 1>\r\n<メモライズ容量補正 10,25,20,10,25>"
Class 104
 Name "Magical Scholar"
 Notes "<要求経験職 103-10>\r\n<職業ランク 2>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 20,35,30,20,35>"
Class 105
 Name "***ダミー"
Class 106
 Name "Alchemist"
 Notes "<要求経験職 103-10>\r\n<職業ランク 2>\r\n<最大TP減少 10%>\r\n<メモライズ容量補正 30,30,30,20,35>"
Class 107
 Name "***ダミー"
Class 108
 Name "Magical Librarian"
 Notes "<要求経験職 103-10>\r\n<職業ランク 2>\r\n<最大TP減少 20%>\r\n<メモライズ容量補正 20,30,40,20,35>"
Class 109
 Name "***ダミー"
Class 110
 Name "Engineer"
 Notes "<転職アイテム 613>\r\n<職業ランク 1>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 20,25,10,20,20>"
Class 111
 Name "Machina Master"
 Notes "<要求経験職 110-10>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 30,35,20,30,30>"
Class 112
 Name "***ダミー"
Class 113
 Name "Spiritualist"
 Notes "<転職アイテム 614>\r\n<職業ランク 1>\r\n<メモライズ容量補正 15,30,25,20,25>"
Class 114
 Name "Necromancer"
 Notes "<要求経験職 113-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 25,40,30,35,35>"
Class 115
 Name "***ダミー"
Class 116
 Name "Medium"
 Notes "<要求経験職 113-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 25,40,35,30,35>"
Class 117
 Name "***ダミー"
Class 118
 Name "Puppeteer"
 Notes "<要求経験職 113-10>\r\n<職業ランク 2>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 30,40,30,30,35>"
Class 119
 Name "***ダミー"
Class 120
 Name "Informant"
 Notes "<転職アイテム 615>\r\n<職業ランク 1>\r\n<メモライズ容量補正 15,25,15,15,25>"
Class 121
 Name "Smooth Talker"
 Notes "<要求経験職 120-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 25,35,25,25,35>"
Class 122
 Name "***ダミー"
Class 123
 Name "Cook"
 Notes "<転職アイテム 616>\r\n<職業ランク 1>\r\n<メモライズ容量補正 20,25,10,20,20>"
Class 124
 Name "Three Star Chef"
 Notes "<要求経験職 123-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 30,35,20,30,30>"
Class 125
 Name "***ダミー"
Class 126
 Name "Nurse"
 Notes "<転職アイテム 617>\r\n<職業ランク 1>\r\n<メモライズ容量補正 10,25,20,15,20>"
Class 127
 Name "Doctor"
 Notes "<要求経験職 126-10>\r\n<職業ランク 2>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 20,35,30,25,30>"
Class 128
 Name "***ダミー"
Class 129
 Name "Maid"
 Notes "<転職アイテム 618>\r\n<職業ランク 1>\r\n<最大TP増加 5%>\r\n<メモライズ容量補正 10,30,20,20,30>"
Class 130
 Name "Master Maid"
 Notes "<要求経験職 129-10>\r\n<職業ランク 2>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 20,40,30,30,40>"
Class 131
 Name "***ダミー"
Class 132
 Name "***ダミー"
Class 133
 Name "***ダミー"
Class 134
 Name "Noble"
 Notes "<転職アイテム 619>\r\n<職業ランク 1>\r\n<メモライズ容量補正 20,20,20,20,25>"
Class 135
 Name "King"
 Notes "<要求経験職 1-10,134-10>\r\n<転職アイテム 620>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 35,35,30,30,40>"
Class 136
 Name "***ダミー"
Class 137
 Name "***ダミー"
Class 138
 Name "Pope"
 Notes "<要求経験職 44-10,134-10>\r\n<転職アイテム 620>\r\n<職業ランク 2>\r\n<メモライズ容量補正 20,40,40,30,40>"
Class 139
 Name "***ダミー"
Class 140
 Name "***ダミー"
Class 141
 Name "Apprentice Hero"
 Notes "<職業ランク 1>\r\n<最大TP増加 10%>\r\n<メモライズ容量補正 25,20,15,20,25>"
Class 142
 Name "Hero"
 Notes "<要求経験職 1-10,141-10>\r\n<転職アイテム 621>\r\n<職業ランク 2>\r\n<最大TP増加 20%>\r\n<メモライズ容量補正 40,35,30,35,40>"
Class 143
 Name "***ダミー"
Class 144
 Name "***ダミー"
Class 145
 Name "***ダミー"
Class 146
 Name "***ダミー"
Class 147
 Name "***ダミー"
Class 148
 Name "***ダミー"
Class 149
 Name "Unemployed"
 Notes "<職業ランク 1>\r\n<メモライズ容量補正 20,20,20,20,20>"
Class 151
 Name "Human"
 Notes "<職業ランク 1>"
Class 152
 Name "Demonoid"
 Notes "<要求経験職 151-10,160-10,365-10>\r\n<転職アイテム 656>\r\n<職業ランク 2>\r\n<HPタイプ消費率 6,50%>\r\n<HPタイプ消費率 7,50%>\r\n<HPタイプ消費率 9,50%>\r\n<HPタイプ消費率 10,50%>\r\n<HPタイプ消費率 14,50%>\r\n<HPタイプ消費率 18,50%>\r\n<HPタイプ消費率 19,50%>\r\n<HPタイプ消費率 21,50%>\r\n<HPタイプ消費率 25,50%>\r\n<HPタイプ消費率 27,50%>\r\n<HPタイプ消費率 29,50%>\r\n<HPタイプ消費率 31,50%>\r\n<HPタイプ消費率 33,50%>\r\n<HPタイプ消費率 40,50%>\r\n<HPタイプ消費率 42,50%>\r\n<HPタイプ消費率 48,50%>\r\n<HPタイプ消費率 50,50%>\r\n<HPタイプ消費率 57,50%>\r\n<HPタイプ消費率 62,50%>"
Class 153
 Name "Archfiend"
 Notes "<要求経験職 48-10,152-10>\r\n<転職アイテム 657>\r\n<職業ランク 3>\r\n<HPタイプ消費率 6,0%>\r\n<HPタイプ消費率 7,0%>\r\n<HPタイプ消費率 9,0%>\r\n<HPタイプ消費率 10,0%>\r\n<HPタイプ消費率 14,0%>\r\n<HPタイプ消費率 18,0%>\r\n<HPタイプ消費率 19,0%>\r\n<HPタイプ消費率 21,0%>\r\n<HPタイプ消費率 25,0%>\r\n<HPタイプ消費率 27,0%>\r\n<HPタイプ消費率 29,0%>\r\n<HPタイプ消費率 31,0%>\r\n<HPタイプ消費率 33,0%>\r\n<HPタイプ消費率 40,0%>\r\n<HPタイプ消費率 42,0%>\r\n<HPタイプ消費率 48,0%>\r\n<HPタイプ消費率 50,0%>\r\n<HPタイプ消費率 57,0%>\r\n<HPタイプ消費率 62,0%>"
Class 154
 Name "***ダミー"
Class 155
 Name "Worm Summoner"
 Notes "<要求経験職 151-10>\r\n<転職アイテム 637>\r\n<職業ランク 2>"
Class 156
 Name "***ダミー"
Class 157
 Name "***ダミー"
Class 158
 Name "Yoma"
 Notes "<職業ランク 1>"
Class 159
 Name "High Yoma"
 Notes "<要求経験職 158-10>\r\n<職業ランク 2>"
Class 160
 Name "Noble Yoma"
 Notes "<要求経験職 159-10>\r\n<転職アイテム 623>\r\n<職業ランク 3>"
Class 161
 Name "Holy Demon"
 Notes "<要求経験職 158-10>\r\n<職業ランク 2>"
Class 162
 Name "***ダミー"
Class 163
 Name "Feral Yoma"
 Notes "<要求経験職 158-10>\r\n<職業ランク 2>\r\n<トリガーステート H,0,20%,30>\r\n<トリガーステート H,0,20%,33>\r\n<トリガーステート H,0,20%,36>\r\n<トリガーステート H,0,20%,39>"
Class 164
 Name "***ダミー"
Class 165
 Name "***ダミー"
Class 166
 Name "***ダミー"
Class 167
 Name "Demi-Human"
 Notes "<職業ランク 1>"
Class 168
 Name "Youkai"
 Notes "<要求経験職 167-10>\r\n<職業ランク 2>\r\n<TP消費率 75%>"
Class 169
 Name "***ダミー"
Class 170
 Name "Oni"
 Notes "<要求経験職 167-10>\r\n<職業ランク 2>\r\n<開始時TP 75%>"
Class 171
 Name "***ダミー"
Class 172
 Name "***ダミー"
Class 173
 Name "***ダミー"
Class 174
 Name "***ダミー"
Class 175
 Name "Imp"
 Notes "<職業ランク 1>"
Class 176
 Name "Succubus"
 Notes "<要求経験職 175-10>\r\n<職業ランク 2>"
Class 177
 Name "***ダミー"
Class 178
 Name "Succubus Monk"
 Notes "<要求経験職 175-10>\r\n<職業ランク 2>"
Class 179
 Name "***ダミー"
Class 180
 Name "Succubus Witch"
 Notes "<要求経験職 175-10>\r\n<職業ランク 2>"
Class 181
 Name "***ダミー"
Class 182
 Name "***ダミー"
Class 183
 Name "***ダミー"
Class 184
 Name "Vampire"
 Notes "<職業ランク 1>\r\n<通常攻撃強化 1-30>\r\n<武器スキル倍率強化 1-6-30>"
Class 185
 Name "Nosferatu"
 Notes "<要求経験職 184-10>\r\n<職業ランク 2>\r\n<通常攻撃強化 1-30>\r\n<武器スキル倍率強化 1-6-30>"
Class 186
 Name "***ダミー"
Class 187
 Name "Vampire Magus"
 Notes "<要求経験職 184-10>\r\n<職業ランク 2>\r\n<通常攻撃強化 1-30>\r\n<武器スキル倍率強化 1-6-30>"
Class 188
 Name "***ダミー"
Class 189
 Name "***ダミー"
Class 190
 Name "***ダミー"
Class 192
 Name "***ダミー"
Class 193
 Name "***ダミー"
Class 194
 Name "Mermaid"
 Notes "<職業ランク 1>\r\n<海地形強化>"
Class 195
 Name "High Mermaid"
 Notes "<要求経験職 194-10>\r\n<職業ランク 2>\r\n<海地形強化>"
Class 196
 Name "***ダミー"
Class 197
 Name "***ダミー"
Class 198
 Name "***ダミー"
Class 199
 Name "Mermaid Merchant"
 Notes "<要求経験職 194-10>\r\n<転職アイテム 601>\r\n<職業ランク 2>\r\n<獲得金額倍率 200%>\r\n<海地形強化>"
Class 200
 Name "***ダミー"
Class 201
 Name "***ダミー"
Class 202
 Name "***ダミー"
Class 203
 Name "Elf"
 Notes "<職業ランク 1>\r\n<森地形強化>"
Class 204
 Name "High Elf"
 Notes "<要求経験職 203-10>\r\n<職業ランク 2>\r\n<森地形強化>"
Class 205
 Name "***ダミー"
Class 206
 Name "Forest Elf"
 Notes "<要求経験職 203-10>\r\n<職業ランク 2>\r\n<タイル補正 1-40>\r\n<森地形超強化>"
Class 207
 Name "***ダミー"
Class 208
 Name "Dark Elf"
 Notes "<要求経験職 203-10>\r\n<職業ランク 2>\r\n<森地形強化>"
Class 209
 Name "***ダミー"
Class 210
 Name "Dullahan"
 Notes "<要求経験職 1-10,203-10>\r\n<職業ランク 2>"
Class 211
 Name "***ダミー"
Class 212
 Name "***ダミー"
Class 213
 Name "***ダミー"
Class 214
 Name "Fairy"
 Notes "<職業ランク 1>\r\n<スロットチャンス 1>"
Class 215
 Name "Wind Spirit"
 Notes "<要求経験職 214-10>\r\n<職業ランク 2>\r\n<スロットチャンス 1>"
Class 216
 Name "***ダミー"
Class 217
 Name "Earth Spirit"
 Notes "<要求経験職 214-10>\r\n<職業ランク 2>\r\n<開始時TP 75%>"
Class 218
 Name "***ダミー"
Class 219
 Name "Dark Fairy"
 Notes "<要求経験職 214-10>\r\n<職業ランク 2>\r\n<獲得アイテム倍率 200%>\r\n<スロットチャンス 2>"
Class 220
 Name "***ダミー"
Class 221
 Name "***ダミー"
Class 222
 Name "***ダミー"
Class 223
 Name "Slime"
 Notes "<職業ランク 1>"
Class 224
 Name "Mega Slime"
 Notes "<要求経験職 223-10>\r\n<職業ランク 2>"
Class 225
 Name "***ダミー"
Class 226
 Name "Heal Slime"
 Notes "<要求経験職 223-10>\r\n<職業ランク 2>"
Class 227
 Name "***ダミー"
Class 228
 Name "Carnivorous Slime"
 Notes "<要求経験職 223-10>\r\n<職業ランク 2>"
Class 229
 Name "***ダミー"
Class 230
 Name "***ダミー"
Class 231
 Name "***ダミー"
Class 232
 Name "Beast"
 Notes "<職業ランク 1>"
Class 233
 Name "Lunatic Beast"
 Notes "<要求経験職 232-10>\r\n<職業ランク 2>"
Class 234
 Name "***ダミー"
Class 235
 Name "Speed Beast"
 Notes "<要求経験職 232-10>\r\n<職業ランク 2>"
Class 236
 Name "***ダミー"
Class 237
 Name "Minotaur"
 Notes "<要求経験職 1-10,232-10>\r\n<転職アイテム 604>\r\n<職業ランク 2>"
Class 238
 Name "***ダミー"
Class 239
 Name "Centaur"
 Notes "<要求経験職 1-10,232-10>\r\n<職業ランク 2>"
Class 240
 Name "***ダミー"
Class 241
 Name "***ダミー"
Class 242
 Name "***ダミー"
Class 243
 Name "***ダミー"
Class 244
 Name "***ダミー"
Class 245
 Name "Kitsune"
 Notes "<職業ランク 1>"
Class 246
 Name "Mid-Rank Kitsune"
 Notes "<要求経験職 245-10>\r\n<職業ランク 2>"
Class 247
 Name "***ダミー"
Class 248
 Name "Feral Kitsune"
 Notes "<要求経験職 245-10>\r\n<職業ランク 2>\r\n<トリガーステート H,0,20%,30>\r\n<トリガーステート H,0,20%,33>\r\n<トリガーステート H,0,20%,36>\r\n<トリガーステート H,0,20%,39>"
Class 249
 Name "***ダミー"
Class 250
 Name "Fox Geisha"
 Notes "<要求経験職 245-10>\r\n<職業ランク 2>"
Class 251
 Name "***ダミー"
Class 252
 Name "***ダミー"
Class 253
 Name "***ダミー"
Class 254
 Name "Lamia"
 Notes "<職業ランク 1>"
Class 255
 Name "Boa Lamia"
 Notes "<要求経験職 254-10>\r\n<職業ランク 2>"
Class 256
 Name "***ダミー"
Class 257
 Name "Lamia Nun"
 Notes "<要求経験職 254-10>\r\n<職業ランク 2>"
Class 258
 Name "***ダミー"
Class 259
 Name "Snake Miko"
 Notes "<要求経験職 254-10>\r\n<職業ランク 2>"
Class 260
 Name "***ダミー"
Class 261
 Name "Medusa"
 Notes "<要求経験職 33-10,254-10>\r\n<職業ランク 2>"
Class 262
 Name "***ダミー"
Class 263
 Name "***ダミー"
Class 264
 Name "***ダミー"
Class 265
 Name "Scylla"
 Notes "<職業ランク 1>"
Class 266
 Name "High Scylla"
 Notes "<要求経験職 265-10>\r\n<職業ランク 2>"
Class 267
 Name "***ダミー"
Class 268
 Name "***ダミー"
Class 269
 Name "***ダミー"
Class 270
 Name "Scylla Maid"
 Notes "<要求経験職 265-10>\r\n<転職アイテム 618>\r\n<職業ランク 2>"
Class 271
 Name "***ダミー"
Class 272
 Name "***ダミー"
Class 273
 Name "***ダミー"
Class 274
 Name "Harpy"
 Notes "<職業ランク 1>"
Class 275
 Name "Wing Harpy"
 Notes "<要求経験職 274-10>\r\n<職業ランク 2>"
Class 276
 Name "***ダミー"
Class 277
 Name "Harpy Knight"
 Notes "<要求経験職 274-10>\r\n<職業ランク 2>"
Class 278
 Name "***ダミー"
Class 279
 Name "Avis"
 Notes "<要求経験職 274-10>\r\n<職業ランク 2>"
Class 280
 Name "***ダミー"
Class 281
 Name "***ダミー"
Class 282
 Name "***ダミー"
Class 283
 Name "Dragon"
 Notes "<職業ランク 1>"
Class 284
 Name "Dragonoid"
 Notes "<要求経験職 283-10>\r\n<職業ランク 2>"
Class 285
 Name "***ダミー"
Class 286
 Name "***ダミー"
Class 287
 Name "Wyvern"
 Notes "<要求経験職 283-10>\r\n<職業ランク 2>"
Class 288
 Name "***ダミー"
Class 289
 Name "Sea Dragon"
 Notes "<要求経験職 283-10>\r\n<職業ランク 2>\r\n<海地形強化>"
Class 290
 Name "***ダミー"
Class 291
 Name "***ダミー"
Class 292
 Name "***ダミー"
Class 293
 Name "Land-Dweller"
 Notes "<職業ランク 1>"
Class 294
 Name "High Land-Dweller"
 Notes "<要求経験職 293-10>\r\n<職業ランク 2>"
Class 295
 Name "***ダミー"
Class 296
 Name "Leech"
 Notes "<要求経験職 293-10>\r\n<職業ランク 2>\r\n<無効化障壁 100>"
Class 297
 Name "***ダミー"
Class 298
 Name "Demon Hunter"
 Notes "<要求経験職 293-10>\r\n<職業ランク 2>\r\n<踏みとどまり 30%>"
Class 299
 Name "***ダミー"
Class 300
 Name "***ダミー"
Class 301
 Name "Sea-Dweller"
 Notes "<職業ランク 1>\r\n<海地形強化>"
Class 302
 Name "Sea Leech"
 Notes "<要求経験職 301-10>\r\n<職業ランク 2>\r\n<海地形強化>"
Class 303
 Name "***ダミー"
Class 304
 Name "Deep-Sea Leech"
 Notes "<要求経験職 301-10>\r\n<職業ランク 2>\r\n<海地形強化>\r\n<深海地形強化>"
Class 305
 Name "***ダミー"
Class 306
 Name "Seadragon Dancer"
 Notes "<要求経験職 301-10>\r\n<職業ランク 2>\r\n<海地形強化>"
Class 307
 Name "***ダミー"
Class 308
 Name "***ダミー"
Class 309
 Name "Insect"
 Notes "<職業ランク 1>\r\n<砂漠地形強化>"
Class 310
 Name "Subterranean Insect"
 Notes "<要求経験職 309-10>\r\n<職業ランク 2>\r\n<砂漠地形強化>"
Class 311
 Name "***ダミー"
Class 312
 Name "Legion"
 Notes "<要求経験職 309-10>\r\n<職業ランク 2>\r\n<砂漠地形強化>"
Class 313
 Name "***ダミー"
Class 314
 Name "Arachne"
 Notes "<要求経験職 33-10,309-10>\r\n<職業ランク 2>"
Class 315
 Name "***ダミー"
Class 317
 Name "***ダミー"
Class 318
 Name "***ダミー"
Class 319
 Name "Alraune"
 Notes "<職業ランク 1>"
Class 320
 Name "High Alraune"
 Notes "<要求経験職 319-10>\r\n<職業ランク 2>"
Class 321
 Name "***ダミー"
Class 322
 Name "War Alraune"
 Notes "<要求経験職 319-10>\r\n<職業ランク 2>"
Class 323
 Name "***ダミー"
Class 324
 Name "Carnivorous Flower"
 Notes "<要求経験職 319-10>\r\n<職業ランク 2>"
Class 325
 Name "***ダミー"
Class 326
 Name "***ダミー"
Class 327
 Name "***ダミー"
Class 328
 Name "Zombie"
 Notes "<職業ランク 1>"
Class 329
 Name "High Undead"
 Notes "<要求経験職 328-10>\r\n<職業ランク 2>"
Class 330
 Name "***ダミー"
Class 331
 Name "Mummy"
 Notes "<要求経験職 328-10>\r\n<職業ランク 2>"
Class 332
 Name "***ダミー"
Class 333
 Name "Zombie Doctor"
 Notes "<要求経験職 328-10>\r\n<転職アイテム 612,617>\r\n<職業ランク 2>"
Class 334
 Name "***ダミー"
Class 335
 Name "***ダミー"
Class 336
 Name "Ghost"
 Notes "<職業ランク 1>"
Class 337
 Name "Evil Mist"
 Notes "<要求経験職 336-10>\r\n<職業ランク 2>"
Class 338
 Name "***ダミー"
Class 339
 Name "Magic Book"
 Notes "<要求経験職 336-10>\r\n<職業ランク 2>"
Class 340
 Name "***ダミー"
Class 341
 Name "Mimic"
 Notes "<要求経験職 336-10>\r\n<職業ランク 2>\r\n<戦闘開始時発動 34,100%>\r\n<洞窟地形強化>"
Class 342
 Name "***ダミー"
Class 343
 Name "***ダミー"
Class 344
 Name "Doll"
 Notes "<職業ランク 1>"
Class 345
 Name "Automata"
 Notes "<要求経験職 344-10>\r\n<職業ランク 2>"
Class 346
 Name "***ダミー"
Class 347
 Name "Vocaloid"
 Notes "<要求経験職 344-10>\r\n<職業ランク 2>"
Class 348
 Name "***ダミー"
Class 349
 Name "Golem"
 Notes "<要求経験職 344-10>\r\n<転職アイテム 638>\r\n<職業ランク 2>\r\n<踏みとどまり 30%>"
Class 350
 Name "***ダミー"
Class 351
 Name "Minor Chaos"
 Notes "<要求経験職 344-10>\r\n<転職アイテム 639>\r\n<職業ランク 2>"
Class 352
 Name "***ダミー"
Class 353
 Name "***ダミー"
Class 354
 Name "Chimera"
 Notes "<職業ランク 1>"
Class 355
 Name "Tri-Chimera"
 Notes "<要求経験職 354-10>\r\n<職業ランク 2>"
Class 356
 Name "***ダミー"
Class 357
 Name "Magical Chimera"
 Notes "<要求経験職 354-10>\r\n<職業ランク 2>"
Class 358
 Name "***ダミー"
Class 359
 Name "Chimeraroid"
 Notes "<要求経験職 354-10>\r\n<職業ランク 2>"
Class 360
 Name "***ダミー"
Class 361
 Name "***ダミー"
Class 362
 Name "Lowly Angel"
 Notes "<職業ランク 1>\r\n<トリガーステート H,0,20%,266>\r\n<トリガーステート H,3,20%,266>"
Class 363
 Name "High Angel"
 Notes "<要求経験職 362-10>\r\n<職業ランク 2>\r\n<トリガーステート H,0,20%,267>\r\n<トリガーステート H,3,20%,267>"
Class 364
 Name "***ダミー"
Class 365
 Name "Fallen Angel"
 Notes "<要求経験職 363-10>\r\n<転職アイテム 623>\r\n<職業ランク 3>\r\n<トリガーステート H,0,20%,268>\r\n<トリガーステート H,3,20%,268>"
Class 366
 Name "Valkyrie"
 Notes "<要求経験職 1-10,362-10>\r\n<職業ランク 2>\r\n<トリガーステート H,0,20%,267>\r\n<トリガーステート H,3,20%,267>"
Class 367
 Name "***ダミー"
Class 368
 Name "Cupid"
 Notes "<要求経験職 52-10,362-10>\r\n<職業ランク 2>\r\n<トリガーステート H,0,20%,267>\r\n<トリガーステート H,3,20%,267>"
Class 369
 Name "***ダミー"
Class 370
 Name "***ダミー"
Class 371
 Name "***ダミー"
Class 372
 Name "***ダミー"
Class 373
 Name "***ダミー"
Class 374
 Name "***ダミー"
Class 381
 Name "成長1"
Class 382
 Name "成長2"
Class 383
 Name "成長3"
Class 384
 Name "成長4"
Class 385
 Name "成長5"
Class 386
 Name "成長6"
Class 387
 Name "成長7"
Class 388
 Name "成長8"
Class 389
 Name "成長9"
Class 390
 Name "成長10"
Class 391
 Name "装備：重装"
Class 392
 Name "装備：中装"
Class 393
 Name "装備：軽装"
Class 394
 Name "装備：魔法中装"
Class 395
 Name "装備：魔法軽装"
Class 396
 Name "装備：万能"
Class 397
 Name "装備：重装・男"
Class 398
 Name "装備：魔法中装・男"
Class 399
 Name "装備：万能・男"
Class 401
 Name "ルカ"
Class 405
 Name "アリス"
Class 412
 Name "シルフ"
Class 413
 Name "ノーム"
Class 426
 Name "イリアス"
Class 441
 Name "プロメスティン"
Class 453
 Name "Slime Girl"
Class 454
 Name "バニースライム"
Class 455
 Name "ナメクジ娘"
Class 456
 Name "インプ1"
Class 457
 Name "インプ2"
Class 458
 Name "インプ3"
Class 459
 Name "マンドラゴラ娘"
Class 460
 Name "犬娘"
Class 461
 Name "ミミズ娘"
Class 462
 Name "ゴブリン娘"
Class 463
 Name "ゴブリン娘"
Class 464
 Name "プチラミア"
Class 465
 Name "プチラミア"
Class 466
 Name "ヴァンパイアガール"
Class 467
 Name "ヴァンパイアガール"
Class 468
 Name "ドラゴンパピー"
Class 469
 Name "ドラゴンパピー"
Class 470
 Name "ネズミ娘"
Class 471
 Name "狼娘"
Class 472
 Name "フェニックス娘"
Class 473
 Name "ヒル娘"
Class 474
 Name "ウサギ娘"
Class 475
 Name "羊娘"
Class 476
 Name "シュリー"
Class 477
 Name "ジェイド"
Class 478
 Name "シェスタ"
Class 479
 Name "ヌルコ"
Class 480
 Name "オーク娘"
Class 481
 Name "ミツバチ娘"
Class 482
 Name "スズメ娘"
Class 483
 Name "ハーピー"
Class 484
 Name "ハーピーツインズ"
Class 485
 Name "ハイスラッグ娘"
Class 486
 Name "なめくじシスター"
Class 487
 Name "スラグスターズ"
Class 488
 Name "ジャックオーランタン"
Class 489
 Name "ローパー娘"
Class 490
 Name "メーダ娘"
Class 491
 Name "キャンドル娘"
Class 492
 Name "イーター"
Class 493
 Name "ライオット"
Class 494
 Name "ルクスル"
Class 495
 Name "ダークエルフ剣"
Class 496
 Name "ダークエルフ召喚"
Class 497
 Name "フェアリー"
Class 498
 Name "ロリカニ娘"
Class 499
 Name "ナマズ娘"
Class 500
 Name "ラフレシア娘"
Class 501
 Name "セントール"
Class 502
 Name "銀狐二尾"
Class 503
 Name "かむろ二尾"
Class 504
 Name "クモ娘"
Class 505
 Name "ミミック娘"
Class 506
 Name "七尾"
Class 507
 Name "ナマコ娘"
Class 508
 Name "貝娘"
Class 509
 Name "カニ娘"
Class 510
 Name "スライムベス娘"
Class 511
 Name "スイカ娘"
Class 512
 Name "ウツボカズラ娘"
Class 513
 Name "ラミア"
Class 514
 Name "ワカメ娘"
Class 515
 Name "クラゲ娘"
Class 516
 Name "イソギンチャク娘"
Class 517
 Name "アンコウ娘"
Class 518
 Name "メイア"
Class 519
 Name "17ページ"
Class 520
 Name "257ページ"
Class 521
 Name "65537ページ"
Class 522
 Name "シスターラミア"
Class 523
 Name "シスキュバス"
Class 524
 Name "アリクイ娘"
Class 525
 Name "グリズリー娘"
Class 526
 Name "実験体XX-7"
Class 527
 Name "リトルバグ"
Class 528
 Name "カニロイド"
Class 529
 Name "ブリュンヒルデ"
Class 530
 Name "ゴースト"
Class 531
 Name "呪いの人形娘"
Class 532
 Name "ゾンビ剣士"
Class 533
 Name "ゾンビ集団"
Class 534
 Name "クロム"
Class 535
 Name "フレデリカ"
Class 536
 Name "エルフ"
Class 537
 Name "ブラウニーズ"
Class 538
 Name "ツインズフェアリー"
Class 539
 Name "フェアリーズ"
Class 540
 Name "タランチュラ娘"
Class 541
 Name "ミノタウロス娘"
Class 542
 Name "バンダースナッチ娘"
Class 543
 Name "エヴァ"
Class 544
 Name "サボレス"
Class 545
 Name "ムカデ娘"
Class 546
 Name "サソリ娘"
Class 547
 Name "サボテン娘"
Class 548
 Name "ダチョウ娘"
Class 549
 Name "ランプの魔女"
Class 550
 Name "ミイラ娘"
Class 551
 Name "コブラ娘"
Class 552
 Name "ネフェルラミアス"
Class 554
 Name "ワニ娘"
Class 555
 Name "サキ"
Class 556
 Name "デビルファイター"
Class 557
 Name "土蜘蛛"
Class 558
 Name "サックボア"
Class 559
 Name "アイアンメイデン"
Class 560
 Name "ワームビレッジャ"
Class 561
 Name "ウーストレル"
Class 562
 Name "ツボ娘"
Class 563
 Name "リリィ"
Class 564
 Name "小鬼"
Class 565
 Name "オナホ娘"
Class 566
 Name "ナーキュバス"
Class 567
 Name "ルシア"
Class 568
 Name "百々目鬼"
Class 569
 Name "リザードシーフ"
Class 570
 Name "リザードソルジャー"
Class 571
 Name "リザードボス"
Class 572
 Name "蜃気楼娘"
Class 573
 Name "アリジゴク娘"
Class 574
 Name "サンドワーム娘"
Class 575
 Name "デザートスキュラ"
Class 576
 Name "ヴィタエ"
Class 577
 Name "ヴェータラ"
Class 578
 Name "ヴァルト"
Class 579
 Name "シニファ"
Class 580
 Name "シャドー"
Class 581
 Name "ガイストビーネ"
Class 582
 Name "キメラホムンクルス"
Class 583
 Name "アイアンメイデン改"
Class 584
 Name "ジャンクドール娘"
Class 585
 Name "ラディオ"
Class 925
 Name "ソニア"
Class 929
 Name "サラ"
Class 931
 Name "サン・イリア王"
Class 933
 Name "グランドノア女王"
Class 940
 Name "アミラ"
Class 941
 Name "ピーハー"
Class 942
 Name "ドーメイア"
Class 1101
 Name "ネロ"
